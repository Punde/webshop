<!DOCTYPE html>

<html>
<?php
  include_once("components/head.php");

 ?>
<body>
<script>
        function display() {
          var firstname = $('#firstname').val();
          var lastname = $('#lastname').val();
          var email = $('#email').val();
          var text = $('#text').val();
          $.ajax({
            url: 'mail.php',
            type: 'POST',
            data: {action: 'contactform', firstname: firstname, lastname: lastname, email: email, text: text },
            success: function(response) {
              alert(response); }
          });
        }
</script>

<?php
  include_once("components/header.php");
  include_once('components/nav.php');
  print_navigation();
?>
<div class="contact">

  <div class="contact_left">
    <?php echo translate('contact_left'); ?>
  </div>


  <div class="contact_right">
      <form id="contact" action="" method="post">
        <h3><?php echo translate('contact_h1') ?></h3>
        <h4><?php echo translate('contact_h2') ?></h4>
        <fieldset>
          <input placeholder="<?php echo translate('contact_l1') ?>" id="firstname" type="text" tabindex="1" required autofocus>
        </fieldset>
        <fieldset>
          <input placeholder="<?php echo translate('contact_l2') ?>" id="lastname" type="text" tabindex="1" required autofocus>
        </fieldset>
        <fieldset>
          <input placeholder="<?php echo translate('contact_l3') ?>" id="email" type="email" tabindex="2" required>
        </fieldset>
        <fieldset>
          <textarea placeholder="<?php echo translate('contact_l4') ?>" id="text" tabindex="3" required></textarea>
        </fieldset>
        <fieldset>
          <input type='button' onclick='display()' value="<?php echo translate('contact_button') ?>" />
        </fieldset>
      </form>
  </div>
</div>




<?php include ("components/footer.php"); ?>

</body>


</html>
