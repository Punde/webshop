<!DOCTYPE HTML>

<?php
require_once("autoloader.php");
include_once("components/head.php");
include_once("functions.php");
?>
<body>
<?php
include_once("components/header.php");
?>
<h2 class="centered"><?php translate("signup")?></h2>
<form id="signup">
<div class="wrapper">
    <div class="left">
        <div class="form-group">
            <label for="firstname" class="required">First Name</label>
            <input type="text"name="firstname" required></span>
        </div>
        <div class="form-group">
            <label for="username" class="required">Username</label>
            <input type="text"name="username" required></span>
        </div>
        <div class="form-group">
            <label for="psw" class="required">Password</label>
            <input type="password" name="psw" required></span>
        </div>

        <div class="form-group">
            <label for="street">Street</label>
            <input type="text" name="street"></span>
        </div>
    </div>
    <div class="right">
        <div class="form-group">
            <label for="lastname" class="required">Last Name</label>
            <input type="text"name="lastname" required></span>
        </div>
        <div class="form-group">
            <label for="email" class="required">Email</label>
            <input type="text" name="email" required></span>
        </div>
        <div class="form-group">
            <label for="psw2" class="required">Repeat Password</label>
            <input type="password" name="psw2" required></span>
        </div>
        <div class="wrapper">
            <div class="form-group left">
                <label for="city">City</label>
                <input type="text" name="city" pattern="[A-z]{1,}"></span>
            </div>
            <div class="form-group right">
                <label for="zip">ZIP</label>
                <input type="text" name="zip" pattern="[\d]{4}" ></span>
            </div>
        </div>
    </div>
</div>


    <button type="submit" name="signup" class="button buttonBlue">Sign up!</button>

</form>



</body>
<script>

  $('button[name=signup]').click(function(e) {
      e.preventDefault();
      var psw = $("input[name='psw']").val();
      if(checkInputs()){
        $.ajax({
            url: 'user_crud.php',
            type: 'POST',
            data: {
                action: 'signup',
                firstname: $("input[name='firstname']").val(),
                lastname:  $("input[name='lastname']").val(),
                username: $("input[name='username']").val(),
                psw:  $("input[name='psw']").val(),
                email:  $("input[name='email']").val(),
                street:  $("input[name='street']").val(),
                city:  $("input[name='city']").val(),
                zip:  $("input[name='zip']").val()
            },
            success: function(msg) {
                window.location.href = './index.php';
            }
        });
      }else{
          alert("Fill the form correctly");
      }
});

function checkInputs(){
    var firstname = $("input[name='firstname']").val();
    var lastname =  $("input[name='lastname']").val();
    var username = $("input[name='username']").val();
    var psw =  $("input[name='psw']").val();
    var psw2 = $("input[name='psw2']").val();
    var email =  $("input[name='email']").val();
    var street =  $("input[name='street']").val();
    var city =  $("input[name='city']").val();
    var zip =  $("input[name='zip']").val();

    var valid = true;
    valid = valid && firstname.match("[A-z]{1,}") !== null;
    valid = valid &&  lastname.match("[A-z]{1,}")!== null;
    valid = valid &&  username.match("[A-z\d]{1,}")!== null;
    valid = valid &&  (psw == psw2) && psw.length > 0;
    valid = valid &&  email.match("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,63}")!== null;
    valid = valid &&  street.match("[A-z\d]{1,}")!== null;
    valid = valid &&  city.match("[A-z]{1,}")!== null;
    valid = valid &&  !isNaN(zip);
    return valid;
}

$('input').focus(function() {
  $(this).prev().addClass('stylee');
}).blur(function() {
  if($(this).val())
    {
      $(this).prev().addClass('stylee');
    }
  else
  {
  $(this).prev().removeClass('stylee');
  }
});
</script>
