-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 15, 2018 at 04:55 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `webshop`
--

-- -----------------------------------------------------
-- Schema webshop
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `webshop` ;

-- -----------------------------------------------------
-- Schema webshop
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `webshop` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin ;
USE `webshop` ;

-- --------------------------------------------------------

--
-- Table structure for table `Order`
--
DROP TABLE IF EXISTS `Order` ;
CREATE TABLE IF NOT EXISTS `Order` (
  `idOrder` int(11) NOT NULL,
  `Products` varchar(45) DEFAULT NULL,
  `userFK` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Order`
--

INSERT INTO `Order` (`idOrder`, `Products`, `userFK`) VALUES
(1, '1,2', 1),
(2, '1', 2);

-- --------------------------------------------------------

--
-- Table structure for table `Product`
--
DROP TABLE IF EXISTS `Product` ;
CREATE TABLE IF NOT EXISTS `Product` (
  `idProduct` int(11) NOT NULL,
  `Title` varchar(30) DEFAULT NULL,
  `Price` decimal(5,2) DEFAULT NULL,
  `Desc_en` varchar(255) DEFAULT NULL,
  `Desc_fr` varchar(255) DEFAULT NULL,
  `Desc_de` varchar(255) DEFAULT NULL,
  `ImagePath` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Product`
--

INSERT INTO `Product` (`idProduct`, `Title`, `Price`, `Desc_en`, `Desc_fr`, `Desc_de`, `ImagePath`) VALUES
(1, 'Greyezer', '12.50', 'Gruyere', 'Gruyère', 'Greyerzer', 'img/products/gruyere.jpg'),
(2, 'Emmenthaler', '22.40', 'Emmental', 'Emmental', 'Emmentaler', 'img/products/emmentaler.jpg'),
(3, 'Appenzeller', '6.95', 'Appenzeller', 'Appenzeller', 'Appenzeller', 'img/products/appenzeller.jpg'),
(4, 'Brie', '15.00', 'Brie', 'Brie', 'Brie', 'img/products/brie.jpg'),
(5, 'Camembert', '4.20', 'Camembert', 'Camembert', 'Camembert', 'img/products/camembert.jpg'),
(6, 'Cheddar', '8.30', 'Cheddar', 'Cheddar', 'Cheddar', 'img/products/cheddar.jpg'),
(7, 'Mozzarella', '2.00', 'Mozzarella', 'Mozzarella', 'Mozzarella', 'img/products/mozzarella.jpg'),
(8, 'Raclette', '30.00', 'Raclette', 'Raclette', 'Raclette', 'img/products/raclette.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `User`
--
DROP TABLE IF EXISTS `User` ;
CREATE TABLE IF NOT EXISTS `User` (
  `idUser` int(11) NOT NULL,
  `FirstName` varchar(20) DEFAULT NULL,
  `LastName` varchar(20) DEFAULT NULL,
  `Street` varchar(45) DEFAULT NULL,
  `City` varchar(30) DEFAULT NULL,
  `PLZ` int(11) DEFAULT NULL,
  `UserName` varchar(64) NOT NULL,
  `Password` varchar(255) NOT NULL, 
  `Email` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `User`
--

INSERT INTO `User` (`idUser`, `FirstName`, `LastName`, `Street`, `City`, `PLZ`, `UserName`, `Password`, `Email`) VALUES
(1, 'Lukas', 'Müller', 'Strasse 3', 'Bern', 3400, 'test', '1', 'test@bfh.ch'),
(2, 'Peter', 'Pettersson', 'Baumhaus 2', 'Solothurn', 5342, 'a', 'b', 'schlb2@students.bfh.ch');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Order`
--
ALTER TABLE `Order`
  ADD PRIMARY KEY (`idOrder`),
  ADD KEY `user_order_idx` (`userFK`);

--
-- Indexes for table `Product`
--
ALTER TABLE `Product`
  ADD PRIMARY KEY (`idProduct`);

--
-- Indexes for table `User`
--
ALTER TABLE `User`
  ADD PRIMARY KEY (`idUser`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Order`
--
ALTER TABLE `Order`
  MODIFY `idOrder` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `Product`
--
ALTER TABLE `Product`
  MODIFY `idProduct` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `User`
--
ALTER TABLE `User`
  MODIFY `idUser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Order`
--
ALTER TABLE `Order`
  ADD CONSTRAINT `user_order` FOREIGN KEY (`userFK`) REFERENCES `User` (`idUser`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
