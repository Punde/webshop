<!DOCTYPE HTML>
<?php
include_once("autoloader.php");
if(session_status() == PHP_SESSION_NONE){
    //session has not started
    session_start();
}

 ?>
 <script>
  function showHideDetails(id){
    var x = document.getElementById(id);
    if (x.style.display === "none") {
      x.style.display = "block";
    } else {
      x.style.display = "none";
    }
  }
  function reloadCart(){
    $("#cart").load(location.href+" #cart>*",""); 
  }

  function removeFromCart(id){
    var data = 'action=remove&id='+id;
    console.log(data);
    $.ajax({
            url: 'order.php',
            data: data,
            type: 'POST',

            success: function(response) {
              reloadCart();
              console.log(response);}
          });
  }

  function addToCart(id) {
          var taste = $('#taste'+id+' :selected').text();
          var quant = $('#quantity'+id+' :selected').text();
          var data = 'action=add&id='+id+'&taste='+taste+'&quantity='+quant;
          $.ajax({
            url: 'order.php',
            data: data,
            type: 'POST',

            success: function(response) {
              reloadCart();
              showHideDetails(id); }
          });
        }
</script>
<div>
  <?php echo translate("cart");?>
  <table style="width:100%">
   <tr>
     <th><?php echo translate("name");?></th>
     <th><?php echo translate("choose_taste");?></th>
     <th><?php echo translate("choose_quantity");?></th>
     <th><?php echo translate("remove");?></th>
   </tr>
   <?php if(isset($_SESSION['order'])){
     foreach ($_SESSION['order'] as $item) {
       ?>
       <tr>
         <td><?php echo $item->getProduct()->getNameCurrentLang(); ?></td>
         <td><?php echo $item->getTaste(); ?></td>
         <td><?php echo $item->getQuantity(); ?></td>
         <td><button type="button" onclick="removeFromCart('<?php echo $item->getId(); ?>')">X</button></td>
       </tr>
  <?php }
    } ?>
 </table>
<button type="button" <?php echo (isset($_SESSION["order"]) ? "enabled" : "disabled"); ?> onclick="window.location.pathname=window.location.pathname+'/../checkout.php'"><?php echo translate("buy_now");?></button>

</div>
