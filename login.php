<?php
require_once("autoloader.php");
session_start();
if(isset($_POST["uname"]) && isset($_POST["psw"])){
    $user = htmlspecialchars($_POST["uname"]);
    $pw = htmlspecialchars($_POST["psw"]);
    $db = DBConnection::getInstance();
    $stmt = $db->prepare("Select * from User Where UserName = ? And Password = ?");
    $stmt->bind_param("ss", $user, $pw);
    $stmt->execute();
    $result = $stmt->get_result();

    if($result->num_rows == 1){
        $row = $result->fetch_assoc();
        $user = new User($row["idUser"]);
        $_SESSION["LoggedUser"] = $user;
        
    }else{
        echo "fail";
    }

}
?>