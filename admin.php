<!DOCTYPE html>
<html>
<?php  
    require_once("autoloader.php");
    include_once("components/head.php");
    include_once("components/header.php");
    include_once('components/nav.php');
 ?>
<body>
    <?php print_navigation();?>

<div class="row">

  <?php
    function displayPWForm(){
        echo '<form  method="post">
        <div class="form-group">
            <label for="psw" class="required">Password</label>
            <input type="password" name="psw" required></span>
        </div>
        <button type="submit">Enter </button>
    </form>';
    }

    function displayAdminPanel(){
        echo "<h3>Users</h3>";
        displayUserTable();
        echo "<h3>Products</h3>";
        displayProductTable();
    }

    function displayProductTable(){
        echo 
        "<table id='productTable' style='width:100%'>
            <tr>
                <th>English</th>
                <th>German</th>
                <th>French</th>
                <th>Price</th>
                <th>Image</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>";

        $db = DBConnection::getInstance();
        $products = $db->Query("Select IdProduct FROM Product");
        if ($products->num_rows > 0) {
            // output data of each row
            while($row = $products->fetch_assoc()) {
                $product = new Product($row["IdProduct"]);
                $product->renderTableEntry();
            }
    
         } 
         
        echo "<tr>
        <td><input type='text' name='english'></td>
        <td><input type='text' name='german'></td>
        <td><input type='text' name='french'></td>
        <td><input type='text' name='price'></td>
        <td><input type='text' name='image'></td>
        <td><input type='hidden' name='id'><button type='button' onclick='saveProduct();'>save</button></td>
        </tr>";

         echo "</table>";
    }

    function displayUserTable(){

        echo 
        "<table id='userTable' style='width:100%'>
            <tr>
                <th>Firstname</th>
                <th>Lastname</th>
                <th>Username</th>
                <th>Email</th>
                <th>Street</th>
                <th>City</th>
                <th>Zip</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>";

        $db = DBConnection::getInstance();
        $users = $db->Query("Select IdUser FROM User");
        if ($users->num_rows > 0) {
            // output data of each row
            while($row = $users->fetch_assoc()) {
                $user = new User($row["IdUser"]);
                $user->renderTableEntry();
            }
    
         } 
         
        echo "<tr>
        <td><input type='text' name='firstname'></td>
        <td><input type='text' name='lastname'></td>
        <td><input type='text' name='username'></td>
        <td><input type='text' name='email'></td>
        <td><input type='text' name='street'></td>
        <td><input type='text' name='city'></td>
        <td><input type='text' name='zip'></td>
        <td><input type='hidden' name='id'><button type='button' onclick='saveUser();'>save</button></td>
        </tr>";

         echo "</table>";
    }


?>

  <section id="admin">
      <?php 
      if((isset($_POST["psw"]) && $_POST["psw"] == "adminpsw") 
      || (isset($_SESSION["admin"]) && $_SESSION["admin"] == true) ){
            $_SESSION["admin"] = true;
            displayAdminPanel();
        }else{ 
            displayPWForm();
        } ?>
  </section>

</div>
<?php include ("components/footer.php"); ?>
<script>

function deleteUser(id){
    $.ajax({
            url: 'user_crud.php',
            type: 'POST',
            data: {
                action: 'delete',
                id:  id
            },
            success: function(msg) {
                $("#userTable").load(location.href+" #userTable>*","");            
            }               
        });
}

function editUser(id){
    var row = $("#userTable").find("#"+id);
    $("input[name='firstname']").val( row.find("td[name='firstname']").html());
    $("input[name='lastname']").val(row.find("td[name='lastname']").html());
    $("input[name='username']").val(row.find("td[name='username']").html());
    $("input[name='email']").val(row.find("td[name='email']").html());
    $("input[name='street']").val(row.find("td[name='street']").html());
    $("input[name='city']").val(row.find("td[name='city']").html());
    $("input[name='zip']").val(row.find("td[name='zip']").html());
    $("input[name='id']").val(id);
}

function saveUser(){
    $.ajax({
            url: 'user_crud.php',
            type: 'POST',
            data: {
                action: 'update',
                firstname: $("input[name='firstname']").val(),
                lastname:  $("input[name='lastname']").val(),
                username: $("input[name='username']").val(),
                id:  $("input[name='id']").val(),
                email:  $("input[name='email']").val(),
                street:  $("input[name='street']").val(),
                city:  $("input[name='city']").val(),
                zip:  $("input[name='zip']").val()
            },
            success: function(msg) {
                $("#userTable").load(location.href+" #userTable>*","");            
            }               
        });
}


function deleteProduct(id){
    $.ajax({
            url: 'product_crud.php',
            type: 'POST',
            data: {
                action: 'delete',
                id:  id
            },
            success: function(msg) {
                $("#productTable").load(location.href+" #productTable>*","");            
            }               
        });
}

function editProduct(id){
    var row = $("#productTable").find("#"+id);
    $("input[name='english']").val( row.find("td[name='english']").html());
    $("input[name='german']").val(row.find("td[name='german']").html());
    $("input[name='french']").val(row.find("td[name='french']").html());
    $("input[name='price']").val(row.find("td[name='price']").html());
    $("input[name='image']").val(row.find("td[name='image']").html());
    $("input[name='id']").val(id);
}

function saveProduct(){
    $.ajax({
            url: 'product_crud.php',
            type: 'POST',
            data: {
                action: 'update',
                english: $("input[name='english']").val(),
                german:  $("input[name='german']").val(),
                french: $("input[name='french']").val(),
                price:  $("input[name='price']").val(),
                image:  $("input[name='image']").val(),
                id:  $("input[name='id']").val()
            },
            success: function(msg) {
                $("#productTable").load(location.href+" #productTable>*","");            
            }               
        });
}


$('input').focus(function() {
  $(this).prev().addClass('stylee');
}).blur(function() {
  if($(this).val())
    {
      $(this).prev().addClass('stylee');
    }
  else
  {
  $(this).prev().removeClass('stylee');
  }
});</script>
</body>
</html>
