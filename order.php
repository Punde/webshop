<?php
require_once("autoloader.php");

session_start();


if(isset($_POST['action']) && isset($_POST['id']) ){
  $action = htmlspecialchars($_POST["action"]);
  $id = htmlspecialchars($_POST['id']);


  if($action == "add" && isset($_POST['taste']) && isset($_POST['quantity'])){
    $taste = htmlspecialchars($_POST['taste']);
    $quantity = htmlspecialchars($_POST['quantity']);
    $orderid = uniqid();

    $order = new Order($orderid, $id, $quantity, $taste);
    if (!isset($_SESSION['order'])){
        $_SESSION['order'] = array();
    }
    array_push($_SESSION['order'],$order);
    echo "Wurde in den Warenkorb gelegt";

  }elseif ($action == "remove" && isset($_SESSION["order"])) {
    $orders = $_SESSION['order'];
    foreach ($orders as $key => $object) {
      if ($object->getId() == $id) {
          unset($orders[$key]);
          echo "removed cart item";
      }
    }
    $_SESSION['order'] = $orders;
  }
}else{
  echo "Error happened";
}






?>
