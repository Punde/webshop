<!DOCTYPE html>
<html>
<?php
  include_once("components/head.php");
  include_once("components/header.php");
  include_once('components/nav.php');
  require_once("autoloader.php");
 ?>
<body>
    <?php print_navigation();?>

<div class="row">
  <?php include_once("components/sidebar.php"); ?>

  <section>
    <?php echo translate("home_1"); ?>
    <div>
    <img src="img/stilleben1.jpg" alt="Käse Stilleben">
  </div>
  </section>

</div>
<?php include ("components/footer.php"); ?>

</body>
</html>
