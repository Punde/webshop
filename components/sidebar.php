<!DOCTYPE HTML>


<aside>
    <?php
require_once("autoloader.php");

if(!isset($_SESSION["LoggedUser"])){

    echo '<form>
    <div class="form-group">
      <label for="uname">'. translate("login_user") . '</label>
      <input type="text"name="uname" required></span>
    </div>
    <div class="form-group">
    <label for="psw">'. translate("login_password") . '</label>
      <input type="password" name="psw" required></span>
    </div>
    <button type="button" name="login" class="button buttonBlue">Login</button>
    <a href="signup.php">'. translate("account") . '</a>

  </form>
  <br>';
}else{
    $user = $_SESSION["LoggedUser"];
    echo '<div id="logout"><p>Welcome ' . $user->firstname.' '. $user->lastname;
    echo '</p><button type="submit" name="logout" class="button buttonBlue">Logout</button></div>';
}

?>
</aside>
<script>

$('input').focus(function() {
  $(this).prev().addClass('stylee');
}).blur(function() {
  if($(this).val())
    {
      $(this).prev().addClass('stylee');
    }
  else
  {
  $(this).prev().removeClass('stylee');
  }
});


  $('button[name=login]').click(function(e) {
    $.ajax({
        url: 'login.php',
        type: 'POST',
        data: {
            uname: $("input[name='uname']").val(),
            psw:  $("input[name='psw']").val()
        },
        success: function(msg) {
          location.reload();
        }
    });
});

$('button[name=logout]').click(function(e) {
    $.ajax({
        url: 'logout.php',
        type: 'POST',
        success: function(msg) {
          location.reload();
        }
    });
});

</script>
