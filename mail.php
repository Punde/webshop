<?php
require('PHPMailer/src/PHPMailer.php');
require('PHPMailer/src/SMTP.php');
require('autoloader.php');
session_start();

if(isset($_POST["action"])){

    //Set up the mail object
    $mail = new PHPMailer\PHPMailer\PHPMailer();
    $mail->isSMTP();
    $mail->Host = 'mail.gmx.net';
    $mail->SMTPAuth = true;
    $mail->Username = 'dominik.meister@gmx.ch';
    $mail->Password = 'Webshop2019';
    $mail->SMTPSecure = 'tls';
    $mail->Port = 587;
    $mail->From = 'dominik.meister@gmx.ch';
    $mail->FromName = 'Dominik Meister';

    if($_POST["action"] == "contactform"){
        $firstname = htmlspecialchars($_POST['firstname']);
        $lastname = htmlspecialchars($_POST['lastname']);
        $email = htmlspecialchars($_POST['email']);
        $text = htmlspecialchars($_POST['text']);
        $mail->addAddress('dominiksamuel.meister@students.bfh.ch', 'Meister');
        $mail->isHTML(true);
        $mail->Subject = 'Neuer Kontakt';
        $mail->Body = $firstname .' '. $lastname . '<br>' . $email . '<br>' . $text;
        if(!$mail->send()) echo 'Sending message failed: ' .$mail->ErrorInfo;
        else echo 'Message has been sent successfully';
    }elseif(isset($_SESSION["order"]) && htmlspecialchars($_POST["action"]) == "order"){
        sendOrderEmail($mail);
    }
    
}

function sendOrderEmail($mail){
    $firstname = htmlspecialchars($_POST['firstname']);
    $lastname = htmlspecialchars($_POST['lastname']);
    $email = htmlspecialchars($_POST['email']);
    $street = htmlspecialchars($_POST['street']);
    $city = htmlspecialchars($_POST['city']);
    $zip = htmlspecialchars($_POST['zip']);
    $giftwrap = htmlspecialchars($_POST['giftwrap']);
    $payment = htmlspecialchars($_POST['payment']);
    $order = htmlspecialchars($_SESSION['order']);

    $body = $firstname .' '. $lastname . '<br>' . $email . '<br>' ;
    $body .= $street. '<br>' . $city. '<br>' . $zip . '<br>' . $giftwrap. '<br>' . $payment. '<br>' ;

    foreach($order as $item){
        $body .= '<br>{' . $item->getProduct()->getNameCurrentLang() . ', ' . $item->getProduct()->getPrice() . ', ';
        $body .= $item->getQuantity() . ', ' . $item->getTaste() . '}';
    }

    $mail->addAddress($email, $lastname);
    $mail->addBCC('benjamin.schlegel@students.bfh.ch', 'schlb2');

    $response = sendMail($mail, 'Cheese order', $body, $email, $lastname);
    if($response == 'true'){
        unset($_SESSION["order"]);
        echo "true";
    }
}

function sendMail($mail, $subject, $body){
    $mail->isHTML(true);
    $mail->Subject = $subject;
    $mail->Body = $body;
    if(!$mail->send()) return 'Sending message failed: ' .$mail->ErrorInfo;
    else return 'true';
}

?>
