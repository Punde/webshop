<?php

class User{

    public $username, $firstname, $lastname, $zip, $street, $city, $id, $email;



    public function __construct($id){
        $db = DBConnection::getInstance();
        $stmt = $db->prepare("Select * from User Where IdUser = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $result = $stmt->get_result();
        if($result->num_rows > 0){
            $row = $result->fetch_assoc();
            $this->id = $id;
            $this->username = $row["UserName"];
            $this->firstname = $row["FirstName"];
            $this->lastname = $row["LastName"];
            $this->zip = $row["PLZ"];
            $this->street = $row["Street"];
            $this->city = $row["City"];
            $this->id = $id;
            $this->email = $row["Email"];
        }
    }

    //Render the table as a row in the admin panel
    public function renderTableEntry(){
        echo 
        "<tr id='$this->id'>
        <td name='firstname'>$this->firstname</td>
        <td name='lastname'>$this->lastname</td>
        <td name='username'>$this->username</td>
        <td name='email'>$this->email</td>
        <td name='street'>$this->street</td>
        <td name='city'>$this->city</td>
        <td name='zip'>$this->zip</td>
        <td><button type='button' onclick='editUser($this->id);'>Edit</button></td>
        <td><button type='button' onclick='deleteUser($this->id);'>Delete</button></td>
        </tr>";
    }


}


?>