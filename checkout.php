<!DOCTYPE html>

<html>

<?php
  include_once("components/head.php");
  include_once("components/header.php");
  include_once('components/nav.php');
  require_once("autoloader.php");
 ?>
<body>

<?php print_navigation();?>

<div class="row">
    <aside id="cart">
        <?php include_once('components/cart.php') ?>
    </aside>
<section>

    <?php

    function getUserData($attribute){
        if(isset($_SESSION["LoggedUser"])){
            $user = $_SESSION["LoggedUser"];
            $data = $user->$attribute;
            if(isset($data)){
                echo htmlspecialchars($data);
            }else{
                echo "";
            }
        }
    }

?>

<div class="chees">
<form id="checkout">
<h2 class="centered"><?php echo translate("confirm_order");?></h2>
<div class="wrapper checkout">
    <div class="left">
        <div class="form-group">
            <label for="firstname" class="required"><?php echo translate("contact_l1"); ?></label>
            <input type="text"name="firstname" value="<?php getUserData("firstname"); ?>" required></span>
        </div>
        <div class="form-group">
            <label for="lastname" class="required"><?php echo translate("contact_l2"); ?></label>
            <input type="text"name="lastname" value="<?php getUserData("lastname"); ?>" required></span>
        </div>
        <div class="form-group">
            <label for="email" class="required"><?php echo translate("contact_l3"); ?></label>
            <input type="text" name="email" value="<?php getUserData("email"); ?>" required></span>
        </div>
        <div class="form-group">
            <label for="street" class="required"><?php echo translate("contact_l5"); ?></label>
            <input type="text" name="street" value="<?php getUserData("street"); ?>" required></span>
        </div>
        <div class="wrapper">
            <div class="form-group left">
                <label for="city" class="required"><?php echo translate("contact_l6"); ?></label>
                <input type="text" name="city" pattern="[A-z]{1,}" value="<?php getUserData("city"); ?>" required></span>
            </div>
            <div class="form-group right">
                <label for="zip" class="required">ZIP</label>
                <input type="text" name="zip" pattern="[\d]{4}"  value="<?php getUserData("zip"); ?>" required></span>
            </div>
        </div>
    </div>
    <div class="right">
        <h4><?php echo translate("payment"); ?></h4>
        <div class="select">
            <select id="payment" name="payment" class="select-text">
                <option value="postfinance" selected>PostFinance</option>
                <option value="mastercard">MasterCard</option>
                <option value="visa">Visa</option>
                <option value="paypal">PayPal</option>
            </select>
            <span class="select-highlight"></span>
            <span class="select-bar"></span>
        </div>
        <div class="giftwrap">
            <label class="checkbox-label">
                <input type="checkbox" id="giftwrap" name="giftwrap">
                <span><?php echo translate("gift"); ?></span>
            </label>
        </div>
        <button type="button" name="checkout" onclick="sendOrder();" class="checkoutButton button buttonBlue">Checkout!</button>
    </div>
    <script>

        $(function() {
            $('input[type="text"]').prev().addClass('stylee');
        });

        $('input').focus(function() {
            $(this).prev().addClass('stylee');
        }).blur(function() {
        if($(this).val())
            {
                $(this).prev().addClass('stylee');
            }
        else
        {
            $(this).prev().removeClass('stylee');
        }
        });

        function sendOrder(e){
            if(checkInputs()){
                var firstname = $("input[name='firstname']").val();
                var lastname =  $("input[name='lastname']").val();
                var username = $("input[name='username']").val();
                var email =  $("input[name='email']").val();
                var street =  $("input[name='street']").val();
                var city =  $("input[name='city']").val();
                var zip =  $("input[name='zip']").val();
                var giftwrap = $('#giftwrap').is(":checked");
                var payment = $("#payment").val();
                var data = {action: 'order', firstname: firstname,
                    lastname: lastname, email: email,
                    street: street, city:city, zip:zip,
                    giftwrap:giftwrap, payment:payment };

                $.ajax({
                    url: 'mail.php',
                    type: 'POST',
                    data: data,
                    success: function(response) {
                        alert("Order successful! Thank you very much.");
                        window.location.pathname = window.location.pathname + '/../index.php';  
                    }
                });

            }else{
                console.log(fail);
            }
        }


        function checkInputs(){
            var firstname = $("input[name='firstname']").val();
            var lastname =  $("input[name='lastname']").val();
            var email =  $("input[name='email']").val();
            var street =  $("input[name='street']").val();
            var city =  $("input[name='city']").val();
            var zip =  $("input[name='zip']").val();
            var giftwrap = $('#giftwrap').is(":checked");
            var payment = $("#payment").val();

            var valid = true;
            valid = valid &&  firstname.match("[A-z]{1,}") !== null;
            valid = valid &&  lastname.match("[A-z]{1,}")!== null;
            valid = valid &&  email.match("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,63}")!== null;
            valid = valid &&  street.match("[A-z\d]{1,}")!== null;
            valid = valid &&  city.match("[A-z]{1,}")!== null;
            valid = valid &&  !isNaN(zip);
            valid = valid &&  giftwrap !== undefined;
            valid = valid &&  payment !== undefined;
            return valid;
        }
    </script>
</form>
</div>

</section>
</div>

<?php include ("components/footer.php"); ?>

</body>


</html>
