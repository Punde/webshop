<?php
require_once("autoloader.php");
require_once("functions.php");
//This class represents a cheese
class Product{
    private $id;
    private $names;
    private $price;
    private $image;


    public function __construct($id){
        $db = DBConnection::getInstance();
        $stmt = $db->prepare("Select * from Product Where IdProduct = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $result = $stmt->get_result();
        if($result->num_rows > 0){
            $row = $result->fetch_assoc();
            $this->names = $this::buildNameArray($row["Desc_en"], $row["Desc_de"], $row["Desc_fr"]);
            $this->id = $id;
            $this->price = $row["Price"];
            $this->image = $row["ImagePath"];
        }
    }

    public function getNameCurrentLang(){
        $lang = GET_LANGUAGE();
        if(isset($this->names)){
            return $this->names[$lang];
        }
        return null;
    }

    public function getPrice(){
        if(isset($this->price)){
            return $this->price;
        }
    }

    public function getImagePath(){
        if(isset($this->names)){
            return $this->image;
        }
        return null;
    }

    public function getNameArray(){
        if(isset($this->names)){
            return $this->names;
        }
        return null;
    }

    //renders the product in the style of the admin panel
    public function renderTableEntry(){
        echo 
        "<tr id='$this->id'>
        <td name='english'>".$this->getNameArray()['en']."</td>
        <td name='german'>".$this->getNameArray()['de']."</td>
        <td name='french'>".$this->getNameArray()['fr']."</td>
        <td name='price'>".$this->getPrice()."</td>
        <td name='image'>".$this->getImagePath()."</td>
        <td><button type='button' onclick='editProduct($this->id);'>Edit</button></td>
        <td><button type='button' onclick='deleteProduct($this->id);'>Delete</button></td>
        </tr>";
    }

    //renders the product in the style for the product page
    public function renderListItem(){
        $submit = translate('show_details');
        $name = $this->getNameCurrentLang();
        $id = $this->id;
        echo "<div  class='lineitem product'>";
        echo "<img src=$this->image />
        <p>$name</p>
        <button  class='button buttonRound buttonBlue' type='button' onclick=\"showHideDetails('$this->id')\"'>&#x23ec;
        </button>";
        echo "<div style='display:none' class='order-details' id='$this->id'>";
        echo $this->displayOrderForm($id);
        
        echo "</div></div>";
    }

    private static function buildNameArray($en, $de, $fr){
        return array(
            "en" => $en,
            "de" => $de,
            "fr" => $fr
        );
    }

    
     function displayOrderForm($id){
        $product = new Product($id);
          echo "<form class='product-details'>";
          $this->displayTasteTypes($id);
          $this->displayQuantity($id);
          $submit = translate('add_cart');
          echo "
            <input class='button buttonBlue' type='button' onclick='addToCart(\"$id\")' value='$submit' />
          </form>
          ";
      }
    
      function displayQuantity($id) {
        $labelText = translate('choose_quantity');
        echo "<div class='select'>
          <select class='select-text' name='quantity' id='quantity$id'>
            <option value='100'>100g</option>
            <option value='250'>250g</option>
            <option value='500'>500g</option>
            <option value='1000'>1kg</option>
            <span class='select-highlight'></span>
			<span class='select-bar'></span>
          </select></div>";
      }
    
      function displayProductImage($product){
        $path = $product->getImagePath();
        if(isset($path)){
          echo "<img src=$path class='product-img' ><br>";
        }
    
      }
    

    
      function displayTasteTypes($id){
        $labelText = translate('choose_taste');
        $mild = translate('mild');
        $sharp = translate('sharp');
        $normal = translate('normal');
        $aromatic = translate('aromatic');
        echo
        "<div class='select'>
          <select class='select-text' name='taste' id='taste$id'>
            <option value='mild'>$mild</option>
            <option value='sharp'>$sharp</option>
            <option value='normal' selected='selected'>$normal</option>
            <option value='aromatic'>$aromatic</option>
            <span class='select-highlight'></span>
			<span class='select-bar'></span>
          </select></div>";
      }

}

?>
