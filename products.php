<!DOCTYPE html>

<html>

<?php
  include_once("components/head.php");
  include_once("components/header.php");
  include_once('components/nav.php');
  require_once("autoloader.php");
 ?>
<body>


<?php print_navigation();?>

<div class="row">
  <aside id="cart">
<?php include_once('components/cart.php') ?>
      </aside>
<section>

<div class="chees">

<?php

    $db = DBConnection::getInstance();
    $products = $db->Query("Select IdProduct FROM Product");
    if ($products->num_rows > 0) {
        // output data of each row
        while($row = $products->fetch_assoc()) {
            $product = new Product($row["IdProduct"]);
            $product->renderListItem();
        }

     } else {
        echo "No products to display";
     }

?>

</div>

</section>
</div>

<?php include ("components/footer.php"); ?>

</body>


</html>
