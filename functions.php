<?php

//return the set language or default to german
function GET_LANGUAGE(){
    return isset($_COOKIE['language']) ? $_COOKIE['language'] : "de";
}


  // The translation function.
function translate($key) {
$language =  GET_LANGUAGE();
    $texts = array
    (
        "page" => array(
            "de" => "Seite",
            "en" => "Page",
            "fr" => "Page"
        ),
        "content" => array(
            "de" => "Willkommen auf der Seite ",
            "en" => "Welcome to page ",
            "fr" => "Bienvenue sur le site"
        ),
        "main_page" => array(
            "de" => "Start",
            "en" => "Home",
            "fr" => "Page d'aqueil"
        ),
        "contact_page" => array(
            "de" => "Kontakt",
            "en" => "Contact",
            "fr" => "Contact"
        ),
        "about_page" => array(
            "de" => "Über uns",
            "en" => "About us",
            "fr" => "De nous"
        ),
        "products_page" => array(
            "de" => "Produkte",
            "en" => "Products",
            "fr" => "Produits"
        ),
        "signup_page" => array(
            "de" => "Registrieren",
            "en" => "Sign up",
            "fr" => "Enregistrer"
        ),
        "admin_page" => array(
            "de" => "Administration",
            "en" => "Administration",
            "fr" => "Administration"
        ),
        "content" => array(
            "de" => "Lorem ipsum auf deutsch",
            "en" => "Lorem ipsum in english",
            "fr" => "Lorem ipsum en francais"
        ),
        "buy_now" => array(
            "de" => "Bestellen",
            "en" => "Order now",
            "fr" => "Commander"
        ),
        "add_cart" => array(
            "de" => "In den Warenkorb",
            "en" => "Add to cart",
            "fr" => "Add to cart"
        ),
        "show_details" => array(
            "de" => "Details anzeigen",
            "en" => "Show details",
            "fr" => "Show details"
        ),
        "mild" => array(
            "de" => "Mild",
            "en" => "Mild",
            "fr" => "Doux"
        ),
        "sharp" => array(
            "de" => "Würzig",
            "en" => "Sharp",
            "fr" => "Piquant"
        ),
        "normal" => array(
            "de" => "Normal",
            "en" => "Normal",
            "fr" => "Normal"
        ),
        "aromatic" => array(
            "de" => "Aromatisch",
            "en" => "Aromatic",
            "fr" => "Aromatique"
        ),
        "choose_taste" => array(
            "de" => "Geschmack",
            "en" => "Taste",
            "fr" => "Gout"
        ),
        "choose_quantity" => array(
          "de" => "Menge",
          "en" => "Quantity",
          "fr" => "Quantité "
        ),
        "contact_left" => array (
          "de" => "<h1>Oder kontaktieren Sie uns Direkt!</h1>
          <h1>Adresse</h1>
            VrenisChäslade<br>
            Käsestrasse 2<br>
            5000 Blauwal<br>

          <h1>Telefon</h1>
          032 222 222 222

          <h1>E-Mail</h1>
          info@vrenisshop.ch",

          "en" => "<h1>Or contact us directly!</h1>
          <h1>Address</h1>
            VrenisChäslade<br>
            Käsestrasse 2<br>
            5000 Blauwal<br>

          <h1>Phone</h1>
          032 222 222 222

          <h1>E-Mail</h1>
          info@vrenisshop.ch",
          "fr" => "<h1> Ou contactez-nous directement!</h1>
          <h1>Adresse</h1>
            VrenisChäslade<br>
            Käsestrasse 2<br>
            5000 Blauwal<br>

          <h1>Téléphone</h1>
          032 222 222 222

          <h1>E-Mail</h1>
          info@vrenisshop.ch"
        ),
        "contact_h1" => array (
          "de" => "Kontaktieren Sie uns Heute!",
          "en" => "Contact us Today!",
          "fr" => "Contactez-nous aujourd'hui"
        ),
        "contact_h2" => array (
          "de" => "Und sie erhalten eine Antwort in kürzester Zeit",
          "en" => "And you  will receive an Answer in no time ",
          "fr" => "Et vous recevrez une réponse en un rien de temps"
        ),
        "contact_l1" => array (
          "de" => "Vorname",
          "en" => "Firstname",
          "fr" => "prenom"
        ),
        "contact_l2" => array (
          "de" => "Nachname",
          "en" => "Lastname",
          "fr" => "nom de famille"
        ),
        "contact_l3" => array (
          "de" => "E-Mail Adresse",
          "en" => "E-Mail Address",
          "fr" => "E-Mail adress"
        ),
        "contact_l4" => array (
          "de" => "Ihre Nachricht...",
          "en" => "Your Message...",
          "fr" => "Votre message..."
        ),
        "contact_l5" => array (
          "de" => "Strasse",
          "en" => "Street",
          "fr" => "Rue"
        ),
        "contact_l6" => array (
          "de" => "Stadt",
          "en" => "City",
          "fr" => "Ville"
        ),
        "payment" => array (
          "de" => "Bezahlmethode",
          "en" => "Paymentmethod",
          "fr" => "Mode de paiement"
        ),
        "login_user" => array (
          "de" => "Benutzername",
          "en" => "Username",
          "fr" => "Identifant"
        ),
        "login_password" => array (
          "de" => "Passwort",
          "en" => "Password",
          "fr" => "Mot de passe"
        ),
        "signup" => array (
          "de" => "Melde dich an für Vrenis Käse Erfahrung",
          "en" => "Sign up for Vrenis Cheese Experience",
          "fr" => "Inscrivez vous pour Vrenis Experience de fromage"

        ),
        "gift" => array (
          "de" => "Verpacken sie diesen Käse als Geschenk",
          "en" => "Wrap this cheese as a gift",
          "fr" => "Emballez cet fromage en cadeau"
        ),
        "contact_button" => array (
          "de" => "Senden",
          "en" => "Send",
          "fr" => "Envoyer"
        ),
        "account" => array (
          "de" => "Melden Sie sich hier an",
          "en" => "Sign up here",
          "fr" => "Connectez vous ici"
        ),
        "home_1" => array (
          "de" => "<h1>Willkommen auf Vrenis Chäslade</h1>
          <br>In unserem Käse Online Shop sind wir einzigartig in unserer Auswahl und Sortiment.
          <br>Lassen sie sich überraschen von unseren Angeboten oder setzen sie sich mit unseren Experten in Verbindung",
          "en" => "<h1>Welcome to Vrenis Chäslade</h1>
          <br>Our Chees Shop has a unique Selection of Products.
          <br>Let you suprise by our offers or contact one of our Experts.",
          "fr" => "<h1>Bienvenue de Vrenis Chäslade</h1>
          <br>Dans notre magasin de fromage en ligne, nous sommes uniques dans notre sélection et notre assortiment de produits.
          <br>Laissez-vous surprendre par nos offres ou contactez nos experts."
        ),
        "about_1" => array (
          "de" => "<h1>Über uns</h1>
          <br>Wir sind ein kleiner aufstrebender Käse Online Shop.
          <br>Unser Shop umfasst 2 Mitarbeiter welche ihnen eine bestmögliche Beratung bieten können.",
          "en" => "<h1>About us</h1>
          <br> We are a small emerging Cheese Online Shop.<br>
          Our shop has 2 employees who can offer you the best possible advice.",
          "fr" => "<h1>De nous</h1>Nous sommes un petit magasin en ligne de fromages émergent.<br>
          Notre magasin a 2 employés qui peuvent vous offrir le meilleur conseil possible."
        ),
        "name" => array (
        "de" => "Name",
        "en" => "Name",
        "fr" => "Nom"
        ),
        "remove" => array (
        "de" => "Entfernen",
        "en" => "Remove",
        "fr" => "Supprimer"
        ),
        "cart" => array (
        "de" => "Warenkorb",
        "en" => "Cart",
        "fr" => "Chariot"
        ),
        "confirm_order" => array (
        "de" => "Bestätige deine Bestellung",
        "en" => "Confirm your Order",
        "fr" => "Accepter votre Ordre"
      )


    );
    if (isset($texts[$key][$language])) {
        return $texts[$key][$language];
    } else {
        return "[$key]";
    }
    }


  ?>
