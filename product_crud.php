<?php
require("autoloader.php");

if(isset($_POST["action"])){

    $action = $_POST["action"];
    $db = DBConnection::getInstance();

    switch($action){
        case "delete":
            delete();
            break;
        case "update":
            update();
            break;
    }
}

function delete(){
    if(isset($_POST["id"])){
        $db = DBConnection::getInstance();
        try{
            $stmt = $db->prepare("DELETE FROM Product WHERE IdProduct = ?;");
            $stmt->bind_param("s", $_POST["id"]); 
            $test = $stmt->execute();
            $result = $stmt->get_result();
        }catch(Exception $e){
            echo $e;
        }
    }
}



function update(){
    if(isset($_POST["id"]) && isset($_POST["english"]) && isset($_POST["german"]) 
    && isset($_POST["french"])  && isset($_POST["price"])  && isset($_POST["image"])){
        $db = DBConnection::getInstance();
        try{
            $stmt = $db->prepare("UPDATE Product SET `Desc_en` = ?,  `Desc_de` = ?, `Desc_fr` = ?, `Price` = ?, `ImagePath` = ? WHERE `IdProduct` = ?;");
            $stmt->bind_param("ssssss", $_POST["english"], $_POST["german"], $_POST["french"], $_POST["price"],$_POST["image"], $_POST["id"]); 
            $test = $stmt->execute();
            $result = $stmt->get_result();
        }catch(Exception $e){
            echo $e;
        }
    }
}



?>