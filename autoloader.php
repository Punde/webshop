<?php

spl_autoload_register(function ($class) {
    $dirs = [
        "./",
        "db/",
        "classes/"
        ];
        // Try to load class
    foreach($dirs as $dir) {
        $file = $dir . $class . ".class.php";
        if(file_exists($file)) {
            require_once($file);
            break;
        }
    }
}
);

?>