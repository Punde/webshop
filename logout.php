<?php

session_start();
if(isset($_SESSION["LoggedUser"])){
    unset($_SESSION["LoggedUser"]);
}
if(isset($_SESSION["admin"])){
    unset($_SESSION["admin"]);
}
session_destroy();

?>