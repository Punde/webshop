<?php
require_once("autoloader.php");

//This class represents an entry in the shopping cart
class Order{

    public $orderid, $product, $quantity, $taste, $prodid;

    public function __construct($orderid, $prodid, $quantity, $taste){
        $this->orderid = $orderid;
        $this->product = new Product($prodid);
        $this->quantity = $quantity;
        $this->taste = $taste;
    }

    function getId(){
      return $this->orderid;
    }

    function getQuantity() {
      return $this->quantity;
    }
    function getTaste() {
      return $this->taste;
    }
    function getProduct() {
      return $this->product;
    }


}


?>
