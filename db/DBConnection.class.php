<?php

class DBConnection extends MySQLi {
    private static $instance = null ;

    private function __construct(){ 
        $credentials = $this->getCredentials();
        $servername = $credentials["servername"];
        $username = $credentials["username"];
        $password = $credentials["password"];
        $dbname = $credentials["dbname"];
        parent::__construct($servername, $username, $password, $dbname);
        $this->set_charset("utf8");
    }

    static function getCredentials(){
        $lines = file('db/credentials.txt');
        $credentials = array();

        foreach($lines as $line) {
            if(empty($line)) continue;
            $entry = explode("=", $line)[0];
            $data = explode("=", $line)[1];
            $credentials[$entry] = trim($data);

        }
        return $credentials;
    }

    public static function getInstance(){
        if (self::$instance == null){
            self::$instance = new self();
        }
        return self::$instance ;
    }

}
?>