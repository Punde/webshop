<?php
require("autoloader.php");

if(isset($_POST["action"])){

    $action = $_POST["action"];
    $db = DBConnection::getInstance();

    switch($action){
        case "delete":
            delete();
            break;
        case "update":
            update();
            break;
        case "signup":
            signup();
            break;
    }
}

function delete(){
    if(isset($_POST["id"])){
        $db = DBConnection::getInstance();
        try{
            $stmt = $db->prepare("DELETE FROM User WHERE IdUser = ?;");
            $stmt->bind_param("s", $_POST["id"]); 
            $stmt->execute();
            $result = $stmt->get_result();
        }catch(Exception $e){
            echo $e;
        }
    }
}



function update(){
    if(isset($_POST["id"]) && isset($_POST["firstname"]) && isset($_POST["lastname"]) 
    && isset($_POST["username"])  && isset($_POST["email"]) 
    && isset($_POST["street"]) && isset($_POST["city"]) && isset($_POST["zip"])){
        $db = DBConnection::getInstance();
        try{
            $stmt = $db->prepare("UPDATE User SET `FirstName` = ?,  `LastName` = ?, `Street` = ?, `City` = ?, `PLZ` = ?, `UserName` = ?, `Email` = ? WHERE `IdUser` = ?;");
            $stmt->bind_param("ssssssss", $_POST["firstname"], $_POST["lastname"], $_POST["street"]
            , $_POST["city"],$_POST["zip"], $_POST["username"], $_POST["email"], $_POST["id"]); 
            $stmt->execute();
            $result = $stmt->get_result();
        }catch(Exception $e){
            echo $e;
        }
    }
}

function signup(){

    if(isset($_POST["firstname"]) && isset($_POST["lastname"]) && isset($_POST["username"]) && isset($_POST["psw"]) && isset($_POST["email"])){
        $db = DBConnection::getInstance();
        try{
            $stmt = $db->prepare("Insert Into User(`FirstName`, `LastName`, `Street`, `City`, `PLZ`, `UserName`, `Password`, `Email`) Values(?,?,?,?,?,?,?,?);");
            $stmt->bind_param("ssssssss", $_POST["firstname"], $_POST["lastname"], $_POST["street"], $_POST["city"],
            $_POST["zip"], $_POST["username"], $_POST["psw"], $_POST["email"]);
            $test = $stmt->execute();
            $result = $stmt->get_result();
        }catch(Exception $e){
            echo $e;
        }

        try{
            $stmt = $db->prepare("Select idUser From User Where Email = ? And UserName = ? And Password = ?");
            $stmt->bind_param("sss", $_POST["email"], $_POST["username"], $_POST["psw"]);
            $test = $stmt->execute();
            $result = $stmt->get_result();
            if($result->num_rows == 1){
                $row = $result->fetch_assoc();
                session_start();
                $user = new User($row["idUser"]);
                $_SESSION["LoggedUser"] = $user;
            }else{
                echo "Was unable to insert user";
            }
        }catch(Exception $e){
            echo $e;
        }
    }else{
        echo "Signup failed";   
    }
}



?>