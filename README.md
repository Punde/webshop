# Webshop Projekt BFH Informatik

This is a school project. 

**Live version** ([external link](https://webshop.punde.net))

**Password** for admin panel: "adminpsw"

### Implemented features
* HTML/CSS: Grundlegende Struktur und Layout
* Mehrsprachigkeit (mindestens zwei Sprachen)
* Benutzer Registrierung/Login
* Formvalidierung (Client/Server)
* Produkte werden aus DB gelesen
* Warenkorb, Bestellung abschicken
* Bestellung per Email verschicken oder in DB speichern
* JavaScript/JQuery für Interaktionen mit dem Benutzer
* AJAX
* DB Credentials in einem Config-File ablegen (eg. XML)
* Apache Konfiguration (Config-Files schützten, HTTPS,…)
* Geschützter Admin-Bereich für die Produkte-/Benutzer



Authors: Benjamin Schlegel, Dominik Meister

01/2019
