<!DOCTYPE html>
<?php

    include_once("functions.php");

    function print_navigation(){
        $languages = array("de", "en", "fr");
        $pages = array(
            "main_page" => "index.php",
            "products_page" => "products.php",
            "about_page" => "about.php",
            "contact_page" => "contact.php",
            "signup_page" => "signup.php",
            "admin_page" => "admin.php"
        );

        echo "<nav>";
        print_pages($pages);
        print_languages($languages);
        echo "</nav>";
    }

    function print_pages($pages){
        foreach($pages as $key => $value){
            $class = strpos($_SERVER['REQUEST_URI'], $value) !== false ? "class=active-nav" : "";
            echo "<a $class href=\"$value\">".translate($key)."</a>";
        }
    }

    function print_languages($languages) {
        $language =   GET_LANGUAGE();
        $urlBase = $_SERVER["REQUEST_URI"];
        foreach ($languages as $lang) {
            $class = $lang == $language ? "active-nav" : "";
            echo "<a class=\"nav-lang $class\" href=\"#\" onClick='changeActiveLang(\"$lang\");'>"
            .strtoupper($lang)."</a>";
        }
    }
?>

<script>
function changeActiveLang(lang){
    Cookies.set('language', lang);
    location.reload();
}
</script>
